import React, { Component } from 'react';
import { View,Image,KeyboardAvoidingView,SafeAreaView } from "react-native";
import { Container, Header, Content, Tab, Tabs, Form, Item, Input,Button,Text, H1,Textarea,Picker,Icon } from 'native-base';
import LogoHeader2 from '../../component/LogoHeader2';
import { Col, Row, Grid } from 'react-native-easy-grid';
//import { ImagePicker,DocumentPicker } from 'expo';
import * as ImagePicker from 'expo-image-picker';
import * as DocumentPicker from 'expo-document-picker';
import * as firebase from 'firebase';
import Moment from 'moment';
import GoogleReCaptcha from 'rn-google-recaptcha-v2';
const siteKey = '6LfZFdkUAAAAALib79ReeaQafUOl3u1ONFH-YO9N';
const baseUrl = 'http://dadan.id';
const firebaseConfig = {
  apiKey: "AIzaSyBMBa8FQQNlW3lzIoGK-qSHI1KNwZzmvj0",
  authDomain: "ppid-48a0a.firebaseapp.com",
  databaseURL: "https://ppid-48a0a.firebaseio.com",
  projectId: "ppid-48a0a",
  storageBucket: "ppid-48a0a.appspot.com"
}

try{
  firebase.initializeApp(firebaseConfig);
}catch(err){
  if (!/already exists/.test(err.message)) {
    console.error('Firebase initialization error', err.stack)
  }
}


export default class Formpengajuan extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected2: '',
      selected3: '',
      image: null,
      dokumen: null,
      dokumen_ext : null,
      dokumen_name : null,
      rincian : '',
      tujuan : '',
      user : '',
      bulan_pengajuan : '',
      tahun_pengajuan: '',
      no_pengajuan : '',
      user_name : '',
      recaptchaViewHeight: 90, //initial default height
      showSubmit : false

    };
  }

  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
        this.setState({user : user});
        firebase.database().ref(`Users/${this.state.user.uid}/`).on('value', (data) => {
          this.setState({ user_name: data.val().nama });
        });
    });   
  }

  romanize(num) {
    if (isNaN(num))
      return NaN;
    var digits = String(+num).split(""),
      key = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM",
        "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC",
        "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"
      ],
      roman = "",
      i = 3;
    while (i--)
      roman = (key[+digits.pop() + (i * 10)] || "") + roman;
    return Array(+digits.join("") + 1).join("M") + roman;
  }

   onValueChange2(value: string) {
    this.setState({
      selected2: value
    });
  }

   onValueChange3(value: string) {
    this.setState({
      selected3: value
    });
  }

  static navigationOptions = {
    headerTitle: <LogoHeader2 />,
    headerStyle: {
      backgroundColor:'white',
      paddingBottom:20,
      height:88
    },
  };

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: false
    });

    if (!result.cancelled) {
      this.setState({ image: result.uri });
    }
  };

  _pickDoc = async () => {
    let result = await DocumentPicker.getDocumentAsync();
    let name_doc = result.name;
    let res = name_doc.split(".");
    let ekstensiDoc = res[res.length - 1];
    if (!result.cancelled) {
      this.setState({ 
        dokumen: result.uri,
        dokumen_name: result.name,
        dokumen_ext: ekstensiDoc
      });
    }
  };

  kirimPengajuan = async () => {
      //cek no pengajuan
      if(this.state.selected2=='')alert('Anda harus pilih cara memperoleh informasi');
      else if (this.state.selected3 == '') alert('Anda harus pilih cara mendapatkan salinan');
      else if (this.state.rincian == '') alert('Isian rincian informasi yang dibutuhkan harus di isi');
      else if (this.state.tujuan == '') alert('Isian tujuan penggunaan harus di isi');
      else if (this.state.dokumen == null) alert('Dokumen upload harus disertakan');
      else if (this.state.image == null) alert('File KTP harus disertakan');
      else {
        let indonesia = require('moment/locale/id');
        Moment.updateLocale('id', indonesia);
        let bulan = parseInt(Moment().format('M'));
        let tahun = Moment().format('Y');

        ///perlu penanganan bila bulan baru dan nomor pengajuan 1, maka child_added di bawah harus di gnti value
        //seterusnya utk nomor pengajuan di atas 1, tipe na di ganti lagi cild added
        await firebase.database().ref(`No-pengajuan/${tahun}/${bulan}/`).orderByKey().limitToLast(1).once('value', (snapshot) => {

          console.log('selesai perintah firebase cek no pengajuan');
          if (snapshot.exists()) {
            //foreach lagi untuk dapatkan nilai nomor saat ini
            snapshot.forEach((childSnapshot) => {
              let no_saat_ini = childSnapshot.val().no;
              let no_baru = parseInt(no_saat_ini) + 1;

              let bulan_romawi = this.romanize(bulan);
              this.setState({
                no_pengajuan: `${no_baru}/PPID.BNP2TKI/${bulan_romawi}/${tahun}`,
                bulan_pengajuan: Moment().format('M'),
                tahun_pengajuan: Moment().format('Y'),
              });
              firebase.database().ref(`No-pengajuan/${tahun}/${bulan}/`).push({
                'no': no_baru
              });
            });

            
          } else {
            console.log('buat no baru');
            firebase.database().ref(`No-pengajuan/${tahun}/${bulan}/`).push({
              'no': '1'
            });
            let bulan_romawi = this.romanize(bulan);
            this.setState({
              no_pengajuan: `1/PPID.BNP2TKI/${bulan_romawi}/${tahun}`,
              bulan_pengajuan: Moment().format('M'),
              tahun_pengajuan: Moment().format('Y'),
            });
          }

          //input pengajuan
          let rincian = this.state.rincian;
          let tujuan = this.state.tujuan;
          let selected2 = this.state.selected2;
          let selected3 = this.state.selected3;
          let no_pengajuan = this.state.no_pengajuan;
          let status = 'Sedang di review';
          let user_id = this.state.user.uid;
          let user_name = this.state.user_name;
          Moment.updateLocale('id', indonesia);
          let waktu = Moment().format('LLL');
          let time = Moment().format('M/D/YYYY');

          firebase.database().ref('Pengajuan2/').push({
            no_pengajuan,
            rincian,
            tujuan,
            selected2,
            selected3,
            waktu,
            status,
            user_id,
            user_name,
            time
          }).then((data) => {
            this.uploadImage(this.state.image, data.key);
            this.uploadDoc(this.state.dokumen, data.key);
            firebase.database().ref('Pengajuan/' + user_id + '/'+data.key).set({
              no_pengajuan,
              rincian,
              tujuan,
              selected2,
              selected3,
              waktu,
              status,
              user_id
            });
            this.props.navigation.navigate('Dashboarduser')
          }).catch((error) => {
            //error callback
            this.setState({
              errorMessage: error.message
            })
          });
        });
      } 

  }

  uploadImage = async (uri,key) =>{
    const blob = await new Promise((resolve, reject) => { 
          var xhr = new XMLHttpRequest(); 
          xhr.onerror = reject; 
          xhr.onreadystatechange = () => { 
              if (xhr.readyState === 4) { 
                resolve(xhr.response); 
              } 
          }; 
          xhr.open('GET', uri, true); 
          xhr.responseType = 'blob'; // convert type 
          xhr.send(null); 
        }) 

      let ref = firebase.storage().ref().child("ktp-pengajuan/" +"ktp-pengajuan-no-"+key);
      return ref.put(blob);
  }

  uploadDoc = async (uri, key) => {
    const blob = await new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      xhr.onerror = reject;
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          resolve(xhr.response);
        }
      };
      xhr.open('GET', uri, true);
      xhr.responseType = 'blob'; // convert type 
      xhr.send(null);
    })

    let ref = firebase.storage().ref().child("Dokumen-pengajuan/" + "dok-pengajuan-no-" + key + "." + this.state.dokumen_ext);
    firebase.database().ref('Pengajuan/' + this.state.user.uid + '/' + key).update({'file_doc': "dok-pengajuan-no-" +key+"."+ this.state.dokumen_ext});
    return ref.put(blob);
  }

  render() {
    let { image } = this.state;
    let { dokumen } = this.state;
    const { recaptchaViewHeight } = this.state;
    return (
      <Container style={{padding:10}}>
          <Content padder>
          <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }} enabled>
            <Form>
              <Textarea onChangeText={rincian=>this.setState({rincian})} rowSpan={5} bordered placeholder="Rincian Informasi yang dibutuhkan" />
              <Textarea onChangeText={tujuan=>this.setState({tujuan})} rowSpan={5} bordered placeholder="Tujuan penggunaan informasi" />

              <View style={{marginVertical:10}}>
                <Text>Cara memperoleh informasi (*)</Text>
                <Item picker>
                  <Picker
                    mode="dropdown"
                    placeholder="Cara memperoleh Informasi"
                    placeholderStyle={{ color: "#bfc6ea" }}
                    placeholderIconColor="#007aff"
                    selectedValue={this.state.selected2}
                    onValueChange={this.onValueChange2.bind(this)}
                  >
                    <Picker.Item label="Tentukan Cara peroleh informasi" value="" />
                    <Picker.Item label="Melihat" value="Melihat" />
                    <Picker.Item label="Membaca" value="Membaca" />
                    <Picker.Item label="Mencatat" value="Mencatat" />
                    <Picker.Item label="Mendengarkan" value="Mendengarkan" />
                    <Picker.Item label="Mendapatkan Salinan Informasi (Soft/Hard Copy)" value="Mendapatkan Salinan" />
                  </Picker>
                </Item>
              </View>

              <View style={{marginVertical:10}}>
                <Text>Cara mendapatkan salinan (*)</Text>
                <Item picker>
                  <Picker
                    mode="dropdown"
                    placeholder="Cara mendapatkan salinan"
                    placeholderStyle={{ color: "#bfc6ea" }}
                    placeholderIconColor="#007aff"
                    selectedValue={this.state.selected3}
                    onValueChange={this.onValueChange3.bind(this)}
                  >
                    <Picker.Item label="Tentukan Cara dapatkan salinan" value="" />
                    <Picker.Item label="Mengambil langsung" value="Mengambil langsung" />
                    <Picker.Item label="Kurir Pos" value="Kurir Pos" />
                    <Picker.Item label="Pos" value="Pos" />
                    <Picker.Item label="Faksimili" value="Faksimili" />
                    <Picker.Item label="Email" value="Email" />
                    <Picker.Item label="Aplikasi" value="Aplikasi" />
                  </Picker>
                </Item>
              </View>
              <Grid>
                <Col>
                  {image && (
                  <Image source={{uri:image}} style={{ width: 100, height: 100 }} />)}
                  <Button full bordered onPress={this._pickImage}>
                    <Text>Upload KTP</Text>
                  </Button>
                </Col>
                <Col>
                  {dokumen && (
                      <View>
                        <Image source={require('../../assets/default-doc.png')} style={{ width: 100, height: 100 }} />
                        <Text>{this.state.dokumen_name}</Text>
                      </View>
                    )}
                    <Button full bordered onPress={this._pickDoc}>
                      <Text>Upload Dokumen</Text>
                    </Button>
                    </Col>
              </Grid>

              <SafeAreaView style={{flex:1}}>
                      <GoogleReCaptcha
                          style={{ height: recaptchaViewHeight }}
                          siteKey={siteKey}
                          url={baseUrl}
                          languageCode="en"
                          onMessage={this.onRecaptchaEvent} />
              </SafeAreaView>

              {this.state.showSubmit &&
              <Button style={{marginTop:20}} full onPress={this.kirimPengajuan}>
                <Text>Kirim Pengajuan</Text>
              </Button>
              }

            </Form>
          </KeyboardAvoidingView>
          </Content>
      </Container>
    );
  }
  onRecaptchaEvent = event => {
    if (event && event.nativeEvent.data) {
        const data = decodeURIComponent(
            decodeURIComponent(event.nativeEvent.data),
        );
        if (data.startsWith('CONTENT_PARAMS:')) {
            let params = JSON.parse(data.substring(15));
            let recaptchaViewHeight = params.visibility === 'visible' ? params.height : 90;
            this.setState({ recaptchaViewHeight });
        } else if (['cancel', 'error', 'expired'].includes(data)) {
            return;
        } else {
            console.log('Verified code from Google', data);
            this.setState({ recaptchaToken: data });
            this.setState({ showSubmit: true });
        }
    }
  };
}