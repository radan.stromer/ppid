import React, { Component } from 'react';
import { View,Image } from "react-native";
import { Container, Header, Content, Tab, Tabs, Form, Item, Input,Button,Text, H1, Card, CardItem, Body } from 'native-base';
import LogoHeader2 from '../../component/LogoHeader2';
import { Col, Row, Grid } from 'react-native-easy-grid';

import * as firebase from 'firebase';
const firebaseConfig = {
  apiKey: "AIzaSyBMBa8FQQNlW3lzIoGK-qSHI1KNwZzmvj0",
  authDomain: "ppid-48a0a.firebaseapp.com",
  databaseURL: "https://ppid-48a0a.firebaseio.com",
  projectId: "ppid-48a0a",
  storageBucket: "ppid-48a0a.appspot.com"
}

try{
  firebase.initializeApp(firebaseConfig);
}catch(err){
  if (!/already exists/.test(err.message)) {
    console.error('Firebase initialization error', err.stack)
  }
}

function Footer1(props) {
  return (<Grid>
            <Col>
              <Button full onPress={() => props.nav.navigate('Jawaban',{pengajuan:props.noaju,user:props.user})}>
                <Text>Lihat Jawaban </Text>
              </Button>
            </Col>
          </Grid>);
}

function Footer2(props) {
  return (<Grid>
            <Col>
              <Button full onPress={() => props.nav.navigate('Jawaban',{pengajuan:props.noaju,user:props.user})}>
                <Text>Lihat Jawaban Keberatan</Text>
              </Button>
            </Col>
          </Grid>);
}

function Footerused(props){
    if(props.status == "Sudah dijawab"){
            return (<Footer1 nav={props.nav} noaju={props.noaju} user={props.user}/>);
    }else if(props.status == "Keberatan sudah dijawab"){
            return (<Footer2 nav={props.nav} noaju={props.noaju} user={props.user} / >);
    } else {
      return (<View><Text>{props.status}</Text></View>);
    }
}


export default class Dashboarduser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user : '',
      listpengajuan : null,
      loading: true
    };
  }

  static navigationOptions = {
    headerTitle: <LogoHeader2 />,
    headerStyle: {
      backgroundColor:'white',
      paddingBottom:20,
      height:88
    },
  };

    async componentDidMount() {
      await firebase.auth().onAuthStateChanged(user => {
          this.setState({user : user.uid});
      });

      if(this.state.user){
        firebase.database().ref('Pengajuan/'+this.state.user).on('value',(snapshot) => {
            let pengajuan = [];
            snapshot.forEach(child => {
                pengajuan.push({
                  rincian: child.val().rincian,
                  waktu: child.val().waktu,
                  tujuan: child.val().tujuan,
                  selected2: child.val().selected2,
                  selected3: child.val().selected3,
                  key: child.key,
                  status: child.val().status,
                  Keberatan : child.val().Keberatan
                });
              });
            this.setState({listpengajuan:pengajuan});
            this.setState({loading:false});
        });
      }
    }

  render() {
    if(this.state.loading){
        return (
          <Text>Sedang memuat....</Text>
        );
      }
    return (
      <Container style={{padding:6}}>
        <Content>
          <Button full onPress={() => this.props.navigation.navigate('Formpengajuan')}>
            <Text>
              Ajukan Permohonan Informasi
            </Text>
          </Button>
          
          {this.state.listpengajuan && this.state.listpengajuan.map((data,key) => {
            return (<Card>
                      <CardItem header bordered>
                        <Text>Pengajuan {data.waktu}</Text>
                      </CardItem>
                      <CardItem bordered>
                        <Body>
                          <Text>
                            Rincian : {data.rincian}
                          </Text>
                          <Text>
                            Tujuan : {data.tujuan}
                          </Text>
                          <Text>
                            Cara mendapatkan salinan : {data.selected3}
                          </Text>
                          <Text>
                            Cara memperoleh : {data.selected2}
                          </Text>
                          
                        </Body>
                          </CardItem>
                          <CardItem footer bordered>
                            <Footerused nav={this.props.navigation} status={data.status} noaju={data.key} user={this.state.user} />
                          </CardItem>
                    </Card>
                    )
          })}
        </Content>
      </Container>
    );
  }
}