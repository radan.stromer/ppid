import React, { Component } from 'react';
import { View,Image } from "react-native";
import { Container, Header, Content, Tab, Tabs, Form, Item, Input,Button,Text, H1 } from 'native-base';
import LogoHeader from '../../component/LogoHeader';
import { Col, Row, Grid } from 'react-native-easy-grid';
import * as firebase from 'firebase';
const firebaseConfig = {
  apiKey: "AIzaSyBMBa8FQQNlW3lzIoGK-qSHI1KNwZzmvj0",
  authDomain: "ppid-48a0a.firebaseapp.com",
  databaseURL: "https://ppid-48a0a.firebaseio.com",
  projectId: "ppid-48a0a",
  storageBucket: "ppid-48a0a.appspot.com"
}

try{
  firebase.initializeApp(firebaseConfig);
}catch(err){
  if (!/already exists/.test(err.message)) {
    console.error('Firebase initialization error', err.stack)
  }
}

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email:'',
      pass:'',
      errorMessage: null ,
      loading:false,
    };
  }

  static navigationOptions = {
    headerTitle: <LogoHeader />,
    headerStyle: {
      backgroundColor:'green',
      paddingBottom:20,
      height:88
    },
  };

  handleLogin = () => {
    this.setState({
      loading: true
    });
    firebase
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.pass)
      .then(() => this.props.navigation.navigate('Dashboarduser'))
      .catch(error => this.setState({ errorMessage: error.message }))
  }

  render() {
    return (
      <Container style={{padding:10}}>
        <Content>
                <Text style={{fontWeight:'bold', fontSize:14,marginBottom:10, textAlign:'justify'}}>Untuk mengajukan permohonan informasi atau pengajuan keberatan, silahkan login.</Text>
              {this.state.errorMessage &&
              <Text style={{ color: 'red' }}>
                {this.state.errorMessage}
              </Text>}
              <Form>
                <Item regular style={{marginBottom:15}}>
                  <Input onChangeText={email=>this.setState({email})} placeholder="Email" keyboardType="email-address"/>
                </Item>
                <Item regular style={{marginBottom:15}}>
                  <Input onChangeText={pass=>this.setState({pass})} placeholder="Password" secureTextEntry />
                </Item>
                <Button full onPress={this.handleLogin}>
                  <Text>
                    Login
                  </Text>
                </Button>
                {this.state.loading &&
              <Text style={{ color: 'green' }}>
                Loading
              </Text>}
              </Form>
              <Grid style={{marginTop:20}}>
                <Col>
                  <Button full bordered onPress={() => this.props.navigation.navigate('Signup')}>
                    <Text>
                      Daftar
                    </Text>
                  </Button>
                </Col>
                <Col>
                  <Button full bordered onPress={() => this.props.navigation.navigate('Lupa')}>
                    <Text>
                      Lupa Password
                    </Text>
                  </Button>
                </Col>
              </Grid>
        </Content>
      </Container>
    );
  }
}