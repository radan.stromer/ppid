import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Container, Content, Text } from 'native-base';
import PDFReader from 'rn-pdf-reader-js';
import { Constants } from 'expo';


export default class Regulasi2 extends React.Component {
  render() {
    return (
      <Container>
        <PDFReader
          source={{uri:'http://dadan.id/UUNomor18Tahun2017.pdf'}}
        />
      </Container>
    );
  }
}