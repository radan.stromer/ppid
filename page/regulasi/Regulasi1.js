import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Container, Content, Text } from 'native-base';
import PDFReader from 'rn-pdf-reader-js';
import { Constants } from 'expo';


export default class Regulasi1 extends React.Component {
  render() {
    return (
      <Container>
        <PDFReader
          source={{uri:'http://dadan.id/53UU-Nomor-14-Tahun-2008-Tentang-Keterbukaan-Informasi-Publik.pdf'}}
        />
      </Container>
    );
  }
}