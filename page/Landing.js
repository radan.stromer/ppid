import React from 'react';
import { StyleSheet, View,Image,ImageBackground } from 'react-native';
import { Container, Header, Content, Button, Text } from 'native-base';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { createStackNavigator, createAppContainer } from "react-navigation";

export default class Landing extends React.Component {

  constructor(props) {
    super(props);
    this.state = { loading: true };
  }

  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf')
    });
    this.setState({ loading: false });
  }
  
  render() {
    if (this.state.loading) {
      return (
        <Text>Sedang memuat....</Text>
      );
    }
    return (
      <View>
        <ImageBackground style={styles.gambar} source={require("../assets/BNP2TKI.jpg")} style={{width: '100%', height: '100%'}}>
           <View style={{height:'50%'}}></View>
           <Text style={{color:'white',textAlign:'center'}}>Selamat datang di halaman awal aplikasi bnp</Text>
           <Button full><Text>Masuk ke aplikasi</Text></Button>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'blue',
    alignItems: 'center',
    justifyContent: 'center',
  },

  gambar: {
    position:'absolute'
  },
});
