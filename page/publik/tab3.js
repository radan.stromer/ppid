import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity,ScrollView } from 'react-native';
import { Container, Content, Text, Button,H3, List, ListItem } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { withNavigation } from 'react-navigation';
import LogoHeader from '../../component/LogoHeader'; 


 class Tab3 extends React.Component {

  static navigationOptions = {
    headerTitle: <LogoHeader />,
    headerStyle: {
      backgroundColor:'green',
      paddingBottom:20,
      height:88
    },
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Container style={{padding:10}}>
      	<ScrollView>
        <List>
          <ListItem itemDivider>
          	<Text style={{fontWeight:'bold'}}>A. Materi Sosialisasi Deputi Bidang Kerjasama Luar Negeri dan Promosi</Text>
          </ListItem>
          <ListItem>
            
              <Text style={{width:'70%'}}>1. Jobs Insfo</Text>
            
            
              <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Jobsinfo')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
                  <Text style={{fontSize:12,textAlign:'center',color:'white'}}>
                    View
                  </Text>
                </TouchableOpacity>
              </View>
            
          </ListItem>

          <ListItem>
            
              <Text style={{width:'70%'}}>2. Upgradding sSkills</Text>
            
            
              <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('UpgradeSkill')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
                  <Text style={{fontSize:12,textAlign:'center',color:'white'}}>
                    View
                  </Text>
                </TouchableOpacity>
              </View>
            
          </ListItem>


          <ListItem itemDivider>
          	<Text style={{fontWeight:'bold'}}>B. Materi Sosialisasi Deputi Bidang Penempatan:</Text>
          </ListItem>
          <ListItem>
            
              <Text style={{width:'70%'}}>1. Program G to G Jepang</Text>
            
            
              <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Ggjapan')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
                  <Text style={{fontSize:12,textAlign:'center',color:'white'}}>
                    View
                  </Text>
                </TouchableOpacity>
              </View>
            
          </ListItem>

          <ListItem>
            
              <Text style={{width:'70%'}}>2. Program G to G Korea Selatan </Text>
            
            
              <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Ggkorea')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
                  <Text style={{fontSize:12,textAlign:'center',color:'white'}}>
                    View
                  </Text>
                </TouchableOpacity>
              </View>
            
          </ListItem>

          <ListItem>
            
              <Text style={{width:'70%'}}>3.  KUR Penempatan PMI</Text>
            
            
              <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Kur')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
                  <Text style={{fontSize:12,textAlign:'center',color:'white'}}>
                    View
                  </Text>
                </TouchableOpacity>
              </View>
            
          </ListItem>

          <ListItem>
            
              <Text style={{width:'70%'}}>4. Layanan Terpadu Satu Atap (LTSA)</Text>
            
            
              <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Terpadu')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
                  <Text style={{fontSize:12,textAlign:'center',color:'white'}}>
                    View
                  </Text>
                </TouchableOpacity>
              </View>
            
          </ListItem>

          <ListItem>
            
              <Text style={{width:'70%'}}>5. Penempatan PMI Berbasis Online</Text>
            
            
              <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Penempatanmi')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
                  <Text style={{fontSize:12,textAlign:'center',color:'white'}}>
                    View
                  </Text>
                </TouchableOpacity>
              </View>
            
          </ListItem>



          <ListItem itemDivider>
          	<Text style={{fontWeight:'bold'}}>C. Materi Sosialisasi Deputi Bidang Perlindungan :</Text>
          </ListItem>
          <ListItem>
            
              <Text style={{width:'70%'}}>1. Bahaya Tindak Pidana Perdagangan Orang (TPPO)</Text>
            
            
              <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Tppo')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
                  <Text style={{fontSize:12,textAlign:'center',color:'white'}}>
                    View
                  </Text>
                </TouchableOpacity>
              </View>
            
          </ListItem>

          <ListItem>
            
              <Text style={{width:'70%'}}>2. Crisis Center </Text>
            
            
              <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Crisiscenter')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
                  <Text style={{fontSize:12,textAlign:'center',color:'white'}}>
                    View
                  </Text>
                </TouchableOpacity>
              </View>
            
          </ListItem>

          <ListItem>
            
              <Text style={{width:'70%'}}>3. Mediasi dan Advokasi untuk meningkatkan perlindungan PMI</Text>
            
            
              <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('MediaAvokasi')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
                  <Text style={{fontSize:12,textAlign:'center',color:'white'}}>
                    View
                  </Text>
                </TouchableOpacity>
              </View>
            
          </ListItem>

          <ListItem>
            
              <Text style={{width:'70%'}}>4. Pelayanan Kepulangan PMI</Text>
            
            
              <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Pkpmi')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
                  <Text style={{fontSize:12,textAlign:'center',color:'white'}}>
                    View
                  </Text>
                </TouchableOpacity>
              </View>
            
          </ListItem>

          <ListItem>
            
              <Text style={{width:'70%'}}>5. Pemberdayaan Terintegrasi Bagi PMI dan Keluarganya</Text>
            
            
              <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Pemberdayaan')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
                  <Text style={{fontSize:12,textAlign:'center',color:'white'}}>
                    View
                  </Text>
                </TouchableOpacity>
              </View>
            
          </ListItem>

          <ListItem>
            
              <Text style={{width:'70%'}}>6. Pencegahan PMI Non Prosedural</Text>
            
            
              <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Pencegahan')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
                  <Text style={{fontSize:12,textAlign:'center',color:'white'}}>
                    View
                  </Text>
                </TouchableOpacity>
              </View>
            
          </ListItem>

          <ListItem>
            
              <Text style={{width:'70%'}}>7.  Penguatan KKBM</Text>
            
            
              <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
              }}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Penguatan')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
                  <Text style={{fontSize:12,textAlign:'center',color:'white'}}>
                    View
                  </Text>
                </TouchableOpacity>
              </View>
            
          </ListItem>
        </List>
        </ScrollView>
      </Container>
    );
  }
}

export default withNavigation(Tab3);