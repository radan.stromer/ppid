import React, { Component } from 'react';
import { Container, Header, Content, Tab, Tabs } from 'native-base';
import LogoHeader2 from '../../component/LogoHeader2';
import Tab1 from './Tab1';
import Tab2 from './tab2';
import Tab3 from './tab3';
import Tab4 from './Tab4';
export default class MainPublik extends React.Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = {
    headerTitle: <LogoHeader2 />,
    headerStyle: {
      backgroundColor:'green',
      paddingBottom:20,
      height:88
    },
  };

  render() {
    return (
      <Container>
        <Tabs style={{borderWidth:1,borderColor:'black',padding:5, }}>
          <Tab heading="Informasi Berkala" textStyle={{fontSize:12, textAlign:'center'}} activeTextStyle={{textAlign:'center',fontSize:12}}  tabStyle={{backgroundColor:'grey'}}  activeTabStyle={{backgroundColor: 'orange'}}>
            <Tab1 />
          </Tab>
          <Tab heading="Informasi Serta Merta" textStyle={{fontSize:12, textAlign:'center'}} activeTextStyle={{textAlign:'center',fontSize:12}}  tabStyle={{backgroundColor:'grey'}}  activeTabStyle={{backgroundColor: 'orange'}}>
            <Tab2 />
          </Tab>
          <Tab heading="Informasi Tersedia Setiap Saat" textStyle={{fontSize:12, textAlign:'center'}} activeTextStyle={{textAlign:'center',fontSize:12}}  tabStyle={{backgroundColor:'grey'}}  activeTabStyle={{backgroundColor: 'orange'}}>
            <Tab3 />
          </Tab>
          {/* <Tab heading="Informasi Dikecualikan" textStyle={{fontSize:12, textAlign:'center'}} activeTextStyle={{textAlign:'center',fontSize:12}}  tabStyle={{backgroundColor:'grey'}}  activeTabStyle={{backgroundColor: 'orange'}}>
            <Tab4 />
          </Tab> */}
        </Tabs>

      </Container>
    );
  }
}