import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import { Container, Content, Text, Button } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { withNavigation } from 'react-navigation';
import LogoHeader from '../../component/LogoHeader'; 


 class Tab4 extends React.Component {

  static navigationOptions = {
    headerTitle: <LogoHeader />,
    headerStyle: {
      backgroundColor:'green',
      paddingBottom:20,
      height:88
    },
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Container style={{padding:10}}>
        <Grid>
          <Col size={1}>
            <Text>A</Text>
          </Col>
          <Col size={4}>
            <Text>Informasi dokumen dikecualikan</Text>
          </Col>
          <Col size={2}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Kecuali')} style={{backgroundColor:'green',borderRadius:2, paddingBottom:2,paddingTop:2,height:20,width:70}}>
                  <Text style={{fontSize:12,textAlign:'center',color:'white'}}>
                    View
                  </Text>
                </TouchableOpacity>
          </Col>
        </Grid>
      </Container>
    );
  }
}

export default withNavigation(Tab4);