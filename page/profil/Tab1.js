import React, { Component } from 'react';
import {Image} from 'react-native';
import { StyleSheet, View, TouchableOpacity,Dimensions } from 'react-native';
import { Container, Content, Text, Button } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { withNavigation } from 'react-navigation';
import LogoHeader from '../../component/LogoHeader'; 
import ImageZoom from 'react-native-image-pan-zoom';


 class Tab1 extends React.Component {

  static navigationOptions = {
    headerTitle: <LogoHeader />,
    headerStyle: {
      backgroundColor:'green',
      paddingBottom:20,
      height:88
    },
  };

  constructor(props) {
    super(props);
  }

  static defaultProps = {
    doAnimateZoomReset: false,
    maximumZoomScale: 2,
    minimumZoomScale: 1,
    zoomHeight: Dimensions.get('window').height,
    zoomWidth: Dimensions.get('window').width,
  }

  render() {
    return (
      <ImageZoom cropWidth={390}
        			 cropHeight={550}
        			 imageWidth={320}
        			 imageHeight={550}
        			 enableSwipeDown={true}>
        <Image style={{ width:290, height:430,top:-10}} source={require('../../assets/struktur-organisasi-PPID.png')} />
      </ImageZoom>
    ); 
  }
}

export default withNavigation(Tab1);