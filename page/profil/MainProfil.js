import React, { Component } from 'react';
import { Container, Header, Content, Tab, Tabs } from 'native-base';
import { createStackNavigator, createAppContainer,createBottomTabNavigator } from "react-navigation";
import LogoHeader from '../../component/LogoHeader';
import Tab1 from './Tab1';
import Tab2 from './tab2';
export default class MainProfil extends React.Component {
  static navigationOptions = {
    headerTitle: <LogoHeader />,
    headerStyle: {
      backgroundColor:'green',
      paddingBottom:20,
      height:88
    },
  };

  render() {
    return (
      <Container>
        <Tabs style={{borderWidth:1,borderColor:'black',padding:5, }}>
          <Tab heading="Struktur PPID" tabStyle={{backgroundColor:'grey'}}  activeTabStyle={{backgroundColor: 'orange'}}>
            <Tab1 />
          </Tab>
          <Tab heading="Daftar Alamat PPID" tabStyle={{backgroundColor:'grey'}}  activeTabStyle={{backgroundColor: 'orange'}}>
            <Tab2 />
          </Tab>
        </Tabs>

      </Container>
    );
  }
}